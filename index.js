const fs = require('fs');
var https = require('https');
const {WebSocketServer} = require('ws');

const privateKey = fs.readFileSync('keys/privkey1.pem', 'utf8');
const certificate = fs.readFileSync('keys/fullchain1.pem', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var httpsServer = https.createServer(credentials);
httpsServer.listen(2096);

const wss = new WebSocketServer({server: httpsServer});

// const wss = new WebSocketServer({port: 2096})

function genID(length){
    return Math.random().toString(16).substr(2, length);
}

function idExists(id){
    for(let client of wss.clients){
        if(id == client.uniqueID) return true;
    }
    return false;
}

function announceNewUser(connection){
    for(let client of wss.clients){
        if(client.uniqueID == connection.uniqueID) continue;
        if(client.room != connection.room) continue;
        client.send(JSON.stringify({
            type: 'newuser',
            id: connection.uniqueID,
            username: connection.username
        }));
    }
}

function announceUserLeft(id, room){
    for(let client of wss.clients){
        if(client.uniqueID == id) continue;
        if(client.room != room) continue;
        client.send(JSON.stringify({
            type: 'userleft',
            id
        }));
    }
}

function getClient(id){
    for(let client of wss.clients){
        if(client.uniqueID == id) return client;
    }
}

function handleClientMessage(msg, connection){
    let parsed;
    try{
        parsed = JSON.parse(msg);
    }catch{
        return;
    }
    let client;
    switch(parsed.type){
        case 'ready':
            if(parsed.username == undefined || parsed.username == ''){
                connection.username = 'Anonymous';
            }else{
                connection.username = parsed.username;
            }
            if(parsed.room == undefined || parsed.room == ''){
                connection.room = 'Lobby';
            }else{
                connection.room = parsed.room;
            }
            announceNewUser(connection);
            break;
        case 'candidate':
            if(parsed.candidate == undefined) return;
            client = getClient(parsed.id);
            if(client == undefined) return;
            if(client.room != connection.room) return;
            client.send(JSON.stringify({
                type: 'candidate',
                id: connection.uniqueID,
                candidate: parsed.candidate
            }));
            break;
        case 'offer':
            if(parsed.offer == undefined) return;
            client = getClient(parsed.id);
            if(client == undefined) return;
            if(client.room != connection.room) return;
            client.send(JSON.stringify({
                type: 'offer',
                id: connection.uniqueID,
                username: connection.username,
                offer: parsed.offer
            }));
            break;
        case 'reconnectoffer':
            if(parsed.offer == undefined) return;
            client = getClient(parsed.id);
            if(client == undefined){
                announceUserLeft(parsed.id, connection.room);
                return;
            }
            if(client.room != connection.room) return;
            client.send(JSON.stringify({
                type: 'reconnectoffer',
                id: connection.uniqueID,
                username: connection.username,
                offer: parsed.offer
            }));
            break;
        case 'answer':
            if(parsed.answer == undefined) return;
            client = getClient(parsed.id);
            if(client == undefined) return;
            if(client.room != connection.room) return;
            client.send(JSON.stringify({
                type: 'answer',
                id: connection.uniqueID,
                answer: parsed.answer
            }));
            break;
        case 'keepalive':
            connection.send(JSON.stringify({
                type: 'keepalive'
            }));
            break;
    }
}

wss.on('connection', function(connection, request){
    let id;
    do{
        id = genID(8);
    }while(idExists(id));
    connection.uniqueID = id;

    connection.on('message', function(msg){
        handleClientMessage(msg, connection);
    });
    connection.on('close', function(){
        announceUserLeft(connection.uniqueID, connection.room);
    });
    connection.send(JSON.stringify({
        type: 'id',
        id
    }));
});
